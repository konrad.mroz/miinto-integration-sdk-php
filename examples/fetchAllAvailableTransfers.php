<?php
require("../src/MiintoRequestsFactory.php");

//To get the authentication token, you firstly need to create the Miinto channel
//This method has to be called every hour, as the lifespan of API token is exactly 60 minutes
createMiintoChannel();


foreach(MIINTO_LOCATIONS_ARRAY as $key => $val) {
    //Every Miinto market is identified with it's own locationId
    //Location ids are available in the MIINTO_LOCATIONS_ARRAY, and taken from the Auth Call (Create Miinto Channel)

    //Firstly we need to fetch the transfers collection from single market
    $result = fetchTransfersCollection($val);
    var_dump($result);

    //Then iterate over the available transfers in "data" array
    foreach($result["data"] as $item) {

        // Every transfer has the transferId, that can be used to accept or decline it
        // You can accept and decline partially - for example one item from the order can be accepted, and another - rejected
        $transferId = $item["id"];

        //The parentOrderId is the ID of an order, which the transfer will be turned into after it is accepted
        //It can be used to "Update Order" request - to add the tracking number, or to create the return on it
        $parentOrderId = $item["parentOrder"]["id"];

        //The customer's information is available in the billingInformation and shippingInformation
        $customersNameAndSurname = $item["billingInformation"]["name"];
    }
}
?>