<?php
require("../src/MiintoRequestsFactory.php");
createMiintoChannel();
foreach(MIINTO_LOCATIONS_ARRAY as $key => $val) {
    //Every Miinto market is identified with it's own locationId
    //Location ids are available in the MIINTO_LOCATIONS_ARRAY, and taken from the Auth Call (Create Miinto Channel)

    $continue = true;
    $offset = 0;
    while($continue){
        $fetchOnlyOrdersWithUpdatedTrackingNumbers = 'true'; // or 'false'
        $result = fetchOrdersCollection($val, $offset, $fetchOnlyOrdersWithUpdatedTrackingNumbers);
        if(count($result["data"]) == 0){
            $continue = false;
        }
        foreach($result["data"] as $item) {
            // The $item is a JSON array of the order, see JSON schema of the "data"
            // https://miintoorderapiv29.docs.apiary.io/#reference/shop-orders/orders-collection/fetch-orders-collection

            var_dump("Checking orders from: " . $val . " " . $item['parentOrder']['createdAt']);

            $year = explode("-",$item['parentOrder']['createdAt'])[0];
            $month = explode("-",$item['parentOrder']['createdAt'])[1];

            // Highlited particular data extracted from the API response:
            $line = $item['id'] . "\t" .
                $item['parentOrder']['id'] .
                "\t" .  $item['parentOrder']['createdAt'] .
                "\t" .  $item['currency'] .
                "\t" .  $item['billingInformation']['name'] .
                "\t" .  $item['billingInformation']['email'] .
                "\t" .  $item['billingInformation']['phone'] .
                "\t" .  $item['billingInformation']['address']['street'] .
                "\t" .  $item['billingInformation']['address']['postcode'] .
                "\t" .  $item['billingInformation']['address']['city'] .
                "\t" .  $item['billingInformation']['address']['country'] .
                "\t" .  $item['shippingInformation']['deliveryAddress']['type'] .
                "\t" .  $item['shippingInformation']['deliveryInfo']['trackingNumber'] .
                "\t" .  $item['shippingInformation']['deliveryInfo']['isInternational'] . "\n";

        }
        $offset = $offset + 50;
    }
}

