<?php
// DISPLAY ERRORS OPTION
define("DEBUG_MODE", 0);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// READ CONFIG FILE
$config_array_filecontent = file_get_contents(dirname(__FILE__) . "\ConfigurationData.json.dist");
$config_array_json = json_decode($config_array_filecontent, true);

// DEFINE VARIABLES
define('MIINTO_IDENTIFIER', $config_array_json['identifier']);
define('MIINTO_SECRET', $config_array_json['secret']);

// DEFINE HOSTS
define('MIINTO_AUTHAPI_HOST', $config_array_json['auth_service_host']);
define('MIINTO_ORDERAPI_HOST', $config_array_json['order_api_host']);
define('MIINTO_STOCKAPI_HOST', $config_array_json['stock_api_host']);
define('MIINTO_PCAPI_HOST' , $config_array_json['pc_api_host']);
define('MIINTO_PMS_HOST', $config_array_json['pms_api_host']);
define('MIINTO_AUTHAPI_URL', "https://" . $config_array_json['auth_service_host']);
define('MIINTO_ORDERAPI_URL', "https://" . $config_array_json['order_api_host']);
define('MIINTO_STOCKAPI_URL', "https://" . $config_array_json['stock_api_host']);
define('MIINTO_PCAPI_URL' , "https://" . $config_array_json['pc_api_host']);
define('MIINTO_PMS_URL', "http://" . $config_array_json['pms_api_host']);

const MIINTO_STATIC_HEADERS = array(
    "Content-Type: application/json",
    "Miinto-Api-Control-Flavour:" . "Miinto-Generic",
    "Miinto-Api-Control-Version:" . "2.8"
);

?>