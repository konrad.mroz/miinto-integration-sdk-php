<?php
require('MiintoConnector.php');

function fetchShopDetails($channelID){
    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => '',
        'HTTP_TYPE' => 'GET',
        'URL_PATH' => '/shops/' . $channelID,
        'QUERY' => ''
    );

    return useRequest($request);
}

function fetchDeclineReasons(){
    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => '',
        'HTTP_TYPE' => 'GET',
        'URL_PATH' => '/decline-reasons',
        'QUERY' => ''
    );

    return useRequest($request);
}


function fetchTransfersCollection($channelID){
    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => '',
        'HTTP_TYPE' => 'GET',
        'URL_PATH' => '/shops/' . $channelID . '/transfers',
        'QUERY' => ''
    );

    return useRequest($request);
}

function fetchTransferDetails($channelID,
                              $transferId){
    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => '',
        'HTTP_TYPE' => 'GET',
        'URL_PATH' => '/shops/' . $channelID . '/transfers/' . $transferId,
        'QUERY' => ''
    );

    return useRequest($request);
}

function updateTransferDetailsAcceptPosition($channelID,
                                             $transferID,
                                             $positionID){
    $body = array(
        "acceptedPositions" => array(
            array(
                "id" => $positionID
            )
        )
    );

    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => json_encode($body) ,
        'HTTP_TYPE' => 'PATCH',
        'URL_PATH' => '/shops/' . $channelID . '/transfers/' . $transferID,
        'QUERY' => ''
    );

    return useRequest($request);
}

function updateTransferDetailsDeclinePosition($channelID,
                                              $transferID,
                                              $positionID,
                                              $miintoItemId,
                                              $declineReasonText,
                                              $declineReasonCode){
    $body = array(
        "declinedPositions" => array(
            array(
                "id" => $positionID,
                "declineReasonCode" => $declineReasonCode,
                "declineReasonText" => $declineReasonText,
                "declineReasonMiintoId" => $miintoItemId
            )
        )
    );

    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => json_encode($body) ,
        'HTTP_TYPE' => 'PATCH',
        'URL_PATH' => '/shops/' . $channelID . '/transfers/' . $transferID,
        'QUERY' => ''
    );

    return useRequest($request);
}

function fetchOrdersCollection($locationId, $offset, $awaitingTrackingNumber){
    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => '',
        'HTTP_TYPE' => 'GET',
        'URL_PATH' => '/shops/' . $locationId . '/orders',
        'QUERY' => 'limit=50&awaitingTrackingNumber='
            . $awaitingTrackingNumber .'&sort=-createdAt&offset=' . $offset
    );

    return useRequest($request);

}

function fetchOrderDetails($locationId, $orderId){
    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => '',
        'HTTP_TYPE' => 'GET',
        'URL_PATH' => '/shops/' . $locationId . '/orders/' . $orderId,
        'QUERY' => ''
    );

    return useRequest($request);

}

function updateOrder($channelID,
                     $orderID,
                     $providerId,
                     $trackingNumber){

    $body = array(
        "shippingInformation" => array(
            array(
                "trackingNumber" => $trackingNumber,
                "providerId" => $providerId
            )
        )
    );

    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => json_encode($body) ,
        'HTTP_TYPE' => 'PATCH',
        'URL_PATH' => '/shops/' . $channelID . '/orders/' . $orderID,
        'QUERY' => ''
    );

    return useRequest($request);

}

function fetchParentOrderDetails($countryCode,
                                 $parentOrderId){

    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => '',
        'HTTP_TYPE' => 'GET',
        'URL_PATH' => '/countries/' . $countryCode . '/orders/' . $parentOrderId,
        'QUERY' => ''
    );

    return useRequest($request);
}

function fetchShippingProviders($channelID,
                                $orderId){
    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => '',
        'HTTP_TYPE' => 'GET',
        'URL_PATH' => '/shops/' . $channelID . "/shipping-providers/orders/" . $orderId,
        'QUERY' => ''
    );

    return useRequest($request);
}

function bookShipping($locationId,
                      $parentOrderId,
                      $weightVariant,
                      $pickupPointId){
    $body = array(
        "variantId" => $weightVariant,
        "pickupPointId" => $pickupPointId

    );

    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => json_encode($body) ,
        'HTTP_TYPE' => 'POST',
        'URL_PATH' => '/shops/' . $locationId . '/orders/' . $parentOrderId . "/shippings",
        'QUERY' => ''
    );

    return useRequest($request);

}

function fetchShippingDocuments($locationId,
                                $parentOrderId,
                                $layout){

    $body = array(
        "layout" => $ $layout,
        "documents" => array(
            "order_info",
            "return_info",
            "label_return",
            "label_shipping",
        )
    );

    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => json_encode($body) ,
        'HTTP_TYPE' => 'POST',
        'URL_PATH' => '/shops/' . $locationId . '/orders/' . $parentOrderId . "/labels",
        'QUERY' => ''
    );

    return useRequest($request);

}

function listAllRma($locationId){

    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => '',
        'HTTP_TYPE' => 'GET',
        'URL_PATH' => '/shops/' . $locationId . '/rmas',
        'QUERY' => ''
    );

    return useRequest($request);

}

function getRma($locationId,
                $rmaId){

    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => '',
        'HTTP_TYPE' => 'GET',
        'URL_PATH' => '/shops/' . $locationId . '/rmas/' . $rmaId,
        'QUERY' => ''
    );


    return useRequest($request);

}

function acceptRma($locationId,
                   $rmaId){

    $body = array(
        "status" => "accepted",
    );

    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => json_encode($body) ,
        'HTTP_TYPE' => 'POST',
        'URL_PATH' => '/shops/' . $locationId . '/rmas/' . $rmaId,
        'QUERY' => ''
    );


    return useRequest($request);
}

function declineRma($locationId,
                    $rmaId,
                    $declineReason){

    $body = array(
        "status" => "declined",
        "reason" => $declineReason
    );

    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => json_encode($body) ,
        'HTTP_TYPE' => 'POST',
        'URL_PATH' => '/shops/' . $locationId . '/rmas/' . $rmaId,
        'QUERY' => ''
    );


    return useRequest($request);
}


function getRmaRequest($locationId,
                       $rmaRequestsId){

    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => '',
        'HTTP_TYPE' => 'GET',
        'URL_PATH' => '/shops/' . $locationId . '/rma-requests/' . $rmaRequestsId,
        'QUERY' => ''
    );


    return useRequest($request);

}

function listAllRmaRequests($locationId){

    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => '',
        'HTTP_TYPE' => 'GET',
        'URL_PATH' => '/shops/' . $locationId . '/rma-requests',
        'QUERY' => ''
    );


    return useRequest($request);

}

function createRmaRequest($locationId,
                          $parentOrderId,
                          $positionId,
                          $quantity){

    $body = array(
        "orderId" => $parentOrderId,
        "returnedPositions" => array(
            array(
                "positionId" => $positionId,
                "quantity" => $quantity
            )
        )
    );

    $request = array(
        'HOST' => MIINTO_ORDERAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => json_encode($body) ,
        'HTTP_TYPE' => 'POST',
        'URL_PATH' => '/shops/' . $locationId . '/rma-requests',
        'QUERY' => ''
    );

    return useRequest($request);

}

function getDominantLocationId($channelID){
    $request = array(
        'HOST' => MIINTO_PCAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' => '',
        'HTTP_TYPE' => 'GET',
        'URL_PATH' => '/locations/' . $channelID . '/aliases',
        'QUERY' => ''
    );

    $result = useRequest($request);
    return $result['data']['0']['id'];
}

function updateStockPrices($channelID,
                           $itemGroupId,
                           $itemBrand,
                           $itemColor,
                           $itemSize,
                           $itemStockLevel, // INTEGER!
                           $itemPriceCurrencyCode,
                           $itemPriceOriginal,
                           $itemPriceNew){

    $dominant_location_id = getDominantLocationId($channelID);

$body = array(
    array(
        "stock" => $itemStockLevel,
        "group" => $itemGroupId,
        "brand" => $itemBrand,
        "color" => $itemColor,
        "size" => $itemSize,
        "prices" => array(
            array(
                "currency" => $itemPriceCurrencyCode,
                "original"=> $itemPriceOriginal,
                "current" => $itemPriceNew
            )
        )
    )
);

    $request = array(
        'HOST' => MIINTO_STOCKAPI_HOST,
        'AUTH_TYPE' => 'MNT-HMAC-SHA256-1-0',
        'PAYLOAD' =>  json_encode($body),
        'HTTP_TYPE' => 'PATCH',
        'URL_PATH' => '/locations/' . $dominant_location_id . '/items',
        'QUERY' => ''
    );

    return useRequest($request);
}
?>