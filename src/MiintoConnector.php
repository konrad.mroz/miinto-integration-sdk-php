<?php
require('../resources/ConfigurationScript.php');

function createMiintoChannel(){
    // SETTING UP REQUEST DATA
    $host = MIINTO_AUTHAPI_URL . '/channels';
    $headers = MIINTO_STATIC_HEADERS;
    $payload = json_encode([
        'identifier' => MIINTO_IDENTIFIER,
        'secret' => MIINTO_SECRET
    ]);

    // SETTING UP CURL
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $host,
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $payload
    ));

    // SENDING THE REQUEST
    $response = curl_exec($curl);

    if(curl_exec($curl) == false) {
        var_dump('Curl error: ' . curl_error($curl));
    }else{
        // GETTING DATA FROM RESPONSE
        $response_json = json_decode($response, true);
        var_dump('CREATING CHANNEL FOR: ' . MIINTO_IDENTIFIER);
        var_dump('AUTHENTICATION STATUS: ' . $response_json['meta']['status']);
        var_dump('VERSION: ' . $response_json['meta']['version']);

        // GETTING LOCATION ID FROM RESPONSE
        $locations_data = $response_json['data']['privileges']['__GLOBAL__']['Store'];
        $locations_array = [];
        foreach($locations_data as $key => $val) {
            array_push($locations_array, $key);
        }

        // SETTING VARIABLES FOR OTHER REQUESTS
        define('MIINTO_LOCATIONS_ARRAY', $locations_array);
        define('MIINTO_AUTH_TOKEN', $response_json['data']['token']);
        define('MIINTO_AUTH_ID', $response_json['data']['id']);
        define('TOKEN_TIMESTAMP', time());
    }
    curl_close($curl);
}

function useRequest($request){
    // CREATING DYNAMIC VARIABLES
    $timestamp = time();
    $seed = rand(0,100);

    // CREATING HEADER_SIG
    $header = MIINTO_AUTH_ID . "\n" . $timestamp . "\n" . $seed . "\n" . $request['AUTH_TYPE'];
    $header_sig = hash('sha256', $header);
    if (DEBUG_MODE == 1){
        echo "======================================================================";
        echo "\nVALUE 1 - HEADER SIGNATURE: miintoAuthdentifier, timestamp, seed, auth type:\n";
        var_dump($header);
        echo "\nVALUE 1 - HEADER SIGNATURE HASHED: miintoAuthdentifier, timestamp, seed, auth type:\n";
        var_dump($header_sig);
    }


    // CREATING PAYLOAD_SIG
    $payload_sig = hash('sha256', $request['PAYLOAD']);
    if (DEBUG_MODE == 1){
        echo "======================================================================\n";
        echo "VALUE 2 - PAYLOAD:\n";
        var_dump($request['PAYLOAD']);
        echo "VALUE 2 - PAYLOAD HASHED:\n";
        var_dump($payload_sig);
    }

    // CREATING RESOURCE_SIG
    $resource = $request['HTTP_TYPE'] . "\n" . $request['HOST'] . "\n" . $request['URL_PATH'] . "\n" . $request['QUERY'];
    $resource_sig = hash('sha256', $resource );
    if (DEBUG_MODE == 1){
        echo "======================================================================\n";
        echo "VALUE 3 - RESOURCE: http method, host, URL, query\n";
        var_dump($resource);
        echo "VALUE 3 - RESOURCE HASHED: http method, host, URL, query\n";
        var_dump($resource_sig);
    }

    // CREATING SIGNATURE
    $signature = $resource_sig . "\n" . $header_sig . "\n" . $payload_sig;
    $signature_sig = hash_hmac('sha256', $resource_sig . "\n" . $header_sig . "\n" . $payload_sig, MIINTO_AUTH_TOKEN);
    if (DEBUG_MODE == 1) {
        echo "======================================================================\n";
        echo "VALUE 4 - SIGNATURE\n";
        var_dump($signature);
        echo "VALUE 4 - SIGNATURE HASHED\n";
        var_dump($signature_sig);
        echo "VALUE 5 - HMAC TOKEN\n";
        var_dump(MIINTO_AUTH_TOKEN);
    }

    // CREATING HEADERS
    $headers = array(
        "Miinto-Api-Auth-ID:" . MIINTO_AUTH_ID,
        "Miinto-Api-Auth-Signature:" . $signature_sig,
        "Miinto-Api-Auth-Timestamp:" . $timestamp,
        "Miinto-Api-Auth-Seed:" . $seed,
        "Miinto-Api-Auth-Type:" . $request['AUTH_TYPE'],
        "Content-Type: application/json-patch+json",
        "Miinto-Api-Control-Flavour:" . "Miinto-Generic",
        "Miinto-Api-Control-Version:" . "2.8"
    );
    if (DEBUG_MODE == 1){
        echo "======================================================================\n";
        echo "VALUE 6 - REQUEST HEADERS\n";
        foreach($headers as $item)
            var_dump($item);
    }

    // CREATING HOST
    $host = "https://" . $request['HOST'] . $request['URL_PATH'] . "?" . $request['QUERY'];
    if (DEBUG_MODE == 1){
        echo "======================================================================\n";
        echo "VALUE 7 - ENDPOINT URL\n";
        var_dump($host);
        echo "END OF DEBUG MODE INFO\n======================================================================\n";
    }

    // SEND REQUEST
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $host,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $request['HTTP_TYPE'],
        CURLOPT_POSTFIELDS => $request['PAYLOAD'],
        CURLOPT_HTTPHEADER => $headers
    ));

    $result = curl_exec($curl);
    curl_close($curl);
    $result_json = json_decode($result, true);

    return $result_json;
}
?>
